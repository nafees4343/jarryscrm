package com.jarrys.com.jarryscrm.Admin;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jarrys.com.jarryscrm.R;


public class AdminDashboardFragment extends Fragment implements View.OnClickListener {

    LinearLayout l_company,l_product,l_salesman;

    public AdminDashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_admin_dashboard, container, false);

        l_company = rootView.findViewById(R.id.l_company);
        l_product = rootView.findViewById(R.id.l_product);
        l_salesman = rootView.findViewById(R.id.l_salesman);

        l_company.setOnClickListener(this);
        l_product.setOnClickListener(this);
        l_salesman.setOnClickListener(this);


        return rootView;
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }


    @Override
    public void onClick(View view) {
        Fragment fragmentName = null;
        switch(view.getId()){
            case R.id.l_product:
                fragmentName = new ViewProductsFragment();
                replaceFragment(fragmentName);
                break;
            case R.id.l_company:
                fragmentName = new ViewCompanyFragment();
                replaceFragment(fragmentName);
                break;
            case R.id.l_salesman:
                fragmentName = new ViewSalesmanFragment();
                replaceFragment(fragmentName);
                break;
        }
    }
}
