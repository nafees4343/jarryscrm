package com.jarrys.com.jarryscrm.Admin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class UpdateProductActivity extends AppCompatActivity implements View.OnClickListener,IPickResult {

    String pid;
    Button btn_updatepadd, btn_updatepcancel;
    EditText et_updatepname, et_updatepquantity, et_updatepprice;
    String updatepname,updatepquantity,updatepprice, imgproduct;
    ImageView iv_updateproductl,iv;
    Bitmap Bmap;
    ByteArrayOutputStream bStream;
    byte[] byteArray;
    File f1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_product);
        if( getIntent().getExtras() != null)
        {
            Intent i = getIntent();
            pid = i.getStringExtra("pid");
            updatepname = i.getStringExtra("productname");
            updatepquantity = i.getStringExtra("productquantity");
            updatepprice = i.getStringExtra("productprice");
            imgproduct = i.getStringExtra("productpicture");

        }

        Log.d("proproproduct","pid; "+ pid +"\nupdatepname: "+updatepname+"\nupdatepquantity: "+updatepquantity+"\nupdatepprice: "+updatepprice+"\nimgproduct: "+imgproduct);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        et_updatepname = findViewById(R.id.et_updatepname);
        et_updatepquantity = findViewById(R.id.et_updatepquantity);
        et_updatepprice = findViewById(R.id.et_updatepprice);

        iv_updateproductl = findViewById(R.id.iv_updateppicture);

        btn_updatepadd = findViewById(R.id.btn_pupdate);
        btn_updatepcancel = findViewById(R.id.btn_pcancel);

        et_updatepname.setText(updatepname);
        et_updatepquantity.setText(updatepquantity);
        et_updatepprice.setText(updatepprice);
        if (imgproduct != null) {

            Picasso.get()
                    .load(imgproduct)
                    .into(new Target() {

                        @Override
                        public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from) {
                            /* Save the bitmap or do something with it here */

                            try {
                                f1 = returnFile(bitmap);
                                Log.d("cscscscsc","ayaya:"+bitmap);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Bmap = bitmap;

                            // Set it in the ImageView
                            iv_updateproductl.setImageBitmap(bitmap);
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {}


                    });



        } else {
            iv_updateproductl.setImageResource(R.drawable.imgaddproduct);
        }

        btn_updatepadd.setOnClickListener(this);
        btn_updatepcancel.setOnClickListener(this);
        iv_updateproductl.setOnClickListener(this);



    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_pupdate:
                senddata();
                break;
            case R.id.btn_pcancel:
                finish();
                break;
            case R.id.iv_updateppicture:
                PickImageDialog.build(new PickSetup()).show(this);
                break;
        }
    }



    public File returnFile(Bitmap bmp) throws IOException {
        File f = new File(getCacheDir(), ".png");
        f.createNewFile();
        //Convert bitmap to byte array
        Bitmap bitmap = bmp;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    public void senddata(){

        updatepname = et_updatepname.getText().toString();
        updatepquantity = et_updatepquantity.getText().toString();
        updatepprice = et_updatepprice.getText().toString();


        if(updatepname.equals("")){
            Toast.makeText(UpdateProductActivity.this, "product name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(updatepquantity.equals("")){
            Toast.makeText(UpdateProductActivity.this, "quantity is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(updatepprice.equals("")){
            Toast.makeText(UpdateProductActivity.this, "price is missing", Toast.LENGTH_SHORT).show();
            return;
        }


        if(Bmap==null){
            Toast.makeText(UpdateProductActivity.this, "Image is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(UpdateProductActivity.this)) {

            ProgressDialogClass.showRoundProgress(UpdateProductActivity.this,"Loading...");

            Log.d("ADDCOMPANYY","pid: "+ pid +"ADDCNAME: "+updatepname+"\nAddCDESCRIPTION: "+updatepquantity+"\nAddCPhone"+updatepprice+"\nfile: "+f1);

            //***********post user list********

            AndroidNetworking.upload(ConfigURL.URL_UPDATE_PRODUCTS)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(UpdateProductActivity.this))
                    .addMultipartParameter("product_id", pid)
                    .addMultipartParameter("name", updatepname)
                    .addMultipartParameter("quantity", updatepquantity)
                    .addMultipartParameter("unit_price", updatepprice)
                    .addMultipartFile("picture", f1)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(UpdateProductActivity.this, "Updated", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(UpdateProductActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(UpdateProductActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            Bmap = r.getBitmap();

            try {
                f1 = returnFile(Bmap);
                Log.d("onpickresult","onpickresult:"+Bmap);

            } catch (IOException e) {
                e.printStackTrace();
            }

            //If you want the Bitmap.
            iv_updateproductl.setImageBitmap(r.getBitmap());

            //Image path
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
