package com.jarrys.com.jarryscrm.Admin;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class AddCompanyActivity extends AppCompatActivity implements View.OnClickListener,IPickResult {

    Button btn_addcadd,btn_addccancel;
    EditText et_addcname,et_addcdescription,et_addcphoneno,et_addcemail,et_addcaddress;
    String addcname,addcdescription,addcphoneno,addcemail,addcaddress,iv_company;
    ImageView iv_addcompanyl,iv;
    Bitmap bitmap;
    ByteArrayOutputStream bStream;
    byte[] byteArray;
    File f1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_company);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        et_addcname = findViewById(R.id.et_addcname);
        et_addcdescription = findViewById(R.id.et_addcdescription);
        et_addcphoneno = findViewById(R.id.et_addcphoneno);
        et_addcemail = findViewById(R.id.et_addcemail);
        et_addcaddress = findViewById(R.id.et_addcaddress);
        iv_addcompanyl = findViewById(R.id.iv_addcpicture);
        btn_addcadd = findViewById(R.id.btn_addcadd);
        btn_addccancel = findViewById(R.id.btn_addccancel);

        btn_addcadd.setOnClickListener(this);
        btn_addccancel.setOnClickListener(this);
        iv_addcompanyl.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_addcadd:
                senddata();
                break;
            case R.id.btn_addccancel:
                finish();
                break;
            case R.id.iv_addcpicture:
                PickImageDialog.build(new PickSetup()).show(this);
                break;
        }
    }

    public File returnFile(Bitmap bmp) throws IOException {
        File f = new File(getCacheDir(), ".png");
        f.createNewFile();
        //Convert bitmap to byte array
        Bitmap bitmap = bmp;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    public void senddata(){

        addcname = et_addcname.getText().toString();
        addcdescription = et_addcdescription.getText().toString();
        addcphoneno = et_addcphoneno.getText().toString();
        addcemail = et_addcemail.getText().toString();
        addcaddress = et_addcaddress.getText().toString();


        if(addcname.equals("")){
            Toast.makeText(AddCompanyActivity.this, "company name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(addcdescription.equals("")){
            Toast.makeText(AddCompanyActivity.this, "description is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(addcphoneno.equals("")){
            Toast.makeText(AddCompanyActivity.this, "phone no is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(addcemail.equals("")){
            Toast.makeText(AddCompanyActivity.this, "email is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(addcaddress.equals("")){
            Toast.makeText(AddCompanyActivity.this, "address name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(bitmap==null){
            Toast.makeText(AddCompanyActivity.this, "Image is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(AddCompanyActivity.this)) {

            ProgressDialogClass.showRoundProgress(AddCompanyActivity.this,"Loading...");

            Log.d("ADDCOMPANYY","ADDCNAME: "+addcname+"\nAddCDESCRIPTION: "+addcdescription+"\nAddCPhone"+addcphoneno+"\nAddress"+addcaddress+addcemail+"\nfile: "+f1);

            //***********post user list********

            AndroidNetworking.upload(ConfigURL.URL_ADD_COMPANIES)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(AddCompanyActivity.this))
                    .addMultipartParameter("company_name", addcname)
                    .addMultipartParameter("company_description", addcdescription)
                    .addMultipartParameter("company_phone", addcphoneno)
                    .addMultipartParameter("company_address", addcaddress)
                    .addMultipartParameter("company_email", addcemail)
                    .addMultipartFile("company_image", f1)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(AddCompanyActivity.this, "Added", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(AddCompanyActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(AddCompanyActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }




    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            bitmap = r.getBitmap();


            try {
                f1 = returnFile(r.getBitmap());
            } catch (IOException e) {
                e.printStackTrace();
            }

            //If you want the Bitmap.
            iv_addcompanyl.setImageBitmap(r.getBitmap());

            //Image path
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            //Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
