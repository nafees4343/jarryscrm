package com.jarrys.com.jarryscrm.AdaptersAdmin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.Admin.UpdateCompanyActivity;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdapterCompaniesListRv extends RecyclerView.Adapter<AdapterCompaniesListRv.MyViewHolder> {

    private ArrayList<CompanyDetails> arrayList ;
    Context acontext;
    public MyAdapterListener onClickListener;

    public interface MyAdapterListener {
        void iconTextViewOnClick(View v, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tv_name, tv_email, tv_phoneno;
        public ImageView iveditcompany,ivdeletecompany;
        ImageView ivcompany ;


        public MyViewHolder(View v){
            super(v);

            //mCardView = (CardView) v.findViewById(R.id.cv_chat_list);
            tv_name= (TextView) v.findViewById(R.id.tv_name);
            tv_email = (TextView) v.findViewById(R.id.tv_email);
            tv_phoneno = (TextView) v.findViewById(R.id.tv_phoneno);
            //ivproduct = v.findViewById(R.id.ivproduct);
            iveditcompany = v.findViewById(R.id.iv_editcompany);
            ivdeletecompany = v.findViewById(R.id.iv_deletecompany);
            ivcompany =  v.findViewById(R.id.ivcompany);

            //itemView.setOnClickListener(this); // bind the listener
            iveditcompany.setOnClickListener(this);
            ivdeletecompany.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            final CompanyDetails c = arrayList.get(getAdapterPosition());
            switch (v.getId()){
                case R.id.iv_editcompany:
                   Intent i = new Intent(acontext,UpdateCompanyActivity.class);
                   i.putExtra("companyid",c.getcid());
                   i.putExtra("companyname",c.getCompany_name());
                   i.putExtra("companyaddress",c.getCompany_address());
                   i.putExtra("companydescription",c.getCompany_description());
                   i.putExtra("companyemail",c.getCompany_email());
                   i.putExtra("companyimage",c.getCompany_image());
                   i.putExtra("companyphoneno",c.getCompany_phone());
                    acontext.startActivity(i);
                    onClickListener.iconTextViewOnClick(v, getAdapterPosition());
                    Log.d("adadadasda","click hua");
                    break;
                case R.id.iv_deletecompany:
                    open(v,c.getcid(),getAdapterPosition());
                    break;
            }

/*
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
*/
        }
    }

    public void open(View view, final String getcid, final int adapterposition){
        AlertDialog alertDialog = null;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(acontext);
        alertDialogBuilder.setMessage("Are you sure, You wanted to make decision");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        cancelRequest(getcid,adapterposition);
                    }
                });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
         alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void cancelRequest(String cid, final int position){
        ProgressDialogClass.showRoundProgress(acontext,"Loading...");
        AndroidNetworking.get(ConfigURL.URL_DELETE_COMPANIES+cid)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(acontext))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("resres",""+response);
                        try {
                            ProgressDialogClass.dismissRoundProgress();
                            if (response.getBoolean("success")) {

                                Toast.makeText(acontext, "deleted", Toast.LENGTH_LONG).show();
                                removeAt(position);

                            } else if (!response.getBoolean("success")) {
                                Toast.makeText(acontext, "some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(acontext, "asd" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void removeAt(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }

    public AdapterCompaniesListRv(Context context, ArrayList<CompanyDetails> arrayList , MyAdapterListener listener) {
        this.arrayList = arrayList;
        acontext = context;
        onClickListener = listener;

    }
    @Override
    public AdapterCompaniesListRv.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carditemcompanieslist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){

        CompanyDetails current = arrayList.get(position);
        Log.d("Data"," name : "+ current.getCompany_name());
        holder.tv_name.setText(current.getCompany_name());
        holder.tv_email.setText(current.getCompany_email());
        holder.tv_phoneno.setText(current.getCompany_phone());

       if (current.getCompany_image() != null) {

                Picasso.get()
                .load(current.getCompany_image())
                .fit()
                .centerCrop()
                .into(holder.ivcompany);


        } else {
            holder.ivcompany.setImageResource(R.drawable.imgcompanydefualt);
        }


    }

    @Override
    public int getItemCount() { return arrayList.size(); }

}
