package com.jarrys.com.jarryscrm.ModelAdmin;

import java.io.Serializable;

public class CompanyDetails implements Serializable {

         public String id,company_name,company_description,company_image,company_phone,company_address,company_email,user_id;

    public CompanyDetails(String id, String company_name, String company_description, String company_image, String company_phone, String company_address, String company_email,String user_id) {
        this.id = id;
        this.company_name = company_name;
        this.company_description = company_description;
        this.company_image = company_image;
        this.company_phone = company_phone;
        this.company_address = company_address;
        this.company_email = company_email;
        this.user_id = user_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getcid() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_description() {
        return company_description;
    }

    public void setCompany_description(String company_description) {
        this.company_description = company_description;
    }

    public String getCompany_image() {
        return company_image;
    }

    public void setCompany_image(String company_image) {
        this.company_image = company_image;
    }

    public String getCompany_phone() {
        return company_phone;
    }

    public void setCompany_phone(String company_phone) {
        this.company_phone = company_phone;
    }

    public String getCompany_address() {
        return company_address;
    }

    public void setCompany_address(String company_address) {
        this.company_address = company_address;
    }

    public String getCompany_email() {
        return company_email;
    }

    public void setCompany_email(String company_email) {
        this.company_email = company_email;
    }
}
