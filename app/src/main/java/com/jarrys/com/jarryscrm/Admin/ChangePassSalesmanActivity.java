package com.jarrys.com.jarryscrm.Admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;

public class ChangePassSalesmanActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_done;
    EditText et_new_password, et_new_confirm_password;
    String sid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass_salesman);

        btn_done = (Button) findViewById(R.id.bt_save);
        et_new_password = (EditText) findViewById(R.id.et_new_password);
        et_new_confirm_password = (EditText) findViewById(R.id.et_new_passwordConfirmation);


        if( getIntent().getExtras() != null)
        {
            Intent i = getIntent();
            sid = i.getStringExtra("salesmanid");

        }

        btn_done.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_save:
                submit();
                break;
        }
    }

    public void submit() {

        if (et_new_password.getText().toString().isEmpty()) {
            et_new_password.setError("New Password Cannot Be Empty");
            requestFocus(et_new_password);
            return;
        }
        if (et_new_confirm_password.getText().toString().isEmpty()) {
            et_new_confirm_password.setError("Confirm Password Cannot Be Empty");
            requestFocus(et_new_confirm_password);
            return;
        }
        if(!et_new_password.getText().toString().equals(et_new_confirm_password.getText().toString()))
        {
            et_new_password.setError("Passwords are not matching");
            requestFocus(et_new_confirm_password);
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(ChangePassSalesmanActivity.this)) {
            ProgressDialogClass.showRoundProgress(this,"Please wait...");
            sendData();
        } else {
            ProgressDialogClass.dismissDottedProgress();
            Toast.makeText(ChangePassSalesmanActivity.this, "Internet not connected", Toast.LENGTH_LONG).show();

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void sendData() {

        Log.d("sidpass","id: "+sid+"\npass:"+et_new_password);

        AndroidNetworking.post(ConfigURL.URL_UPDATE_SALESMANPASSWORD)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(ChangePassSalesmanActivity.this))
                .addBodyParameter("id", sid)
                .addBodyParameter("password", et_new_password.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("resresp","r:"+response);
                        ProgressDialogClass.dismissRoundProgress();
                        String msg = "";
                        boolean success = false;
                        try {
                            success = response.getBoolean("success");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        if(success) {
                            Toast.makeText(ChangePassSalesmanActivity.this, "password changed" + msg, Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(ChangePassSalesmanActivity.this, "" + anError, Toast.LENGTH_LONG).show();

                    }
                });

    }
}
