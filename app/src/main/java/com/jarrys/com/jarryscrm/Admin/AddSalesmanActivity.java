package com.jarrys.com.jarryscrm.Admin;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;

public class AddSalesmanActivity extends AppCompatActivity  implements View.OnClickListener {

    Button btn_addsadd, btn_addscancel;
    EditText et_addsname,et_addcdescription, et_addsemail,et_addspass;
    String addsname,addspass,addsemail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_salesman);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        et_addsname = findViewById(R.id.et_addsname);
        et_addsemail = findViewById(R.id.et_addsemail);
        et_addspass = findViewById(R.id.et_addspass);


        btn_addsadd = findViewById(R.id.btn_addsadd);
        btn_addscancel = findViewById(R.id.btn_addscancel);

        btn_addsadd.setOnClickListener(this);
        btn_addscancel.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_addsadd:
                senddata();
                break;
            case R.id.btn_addscancel:
                finish();
                break;
        }
    }




    public void senddata(){

        addsname = et_addsname.getText().toString();
        addsemail = et_addsemail.getText().toString();
        addspass = et_addspass.getText().toString();



        if(addsname.equals("")){
            Toast.makeText(AddSalesmanActivity.this, "company name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(addsemail.equals("")){
            Toast.makeText(AddSalesmanActivity.this, "email is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(addspass.equals("")){
            Toast.makeText(AddSalesmanActivity.this, "password is missing", Toast.LENGTH_SHORT).show();
            return;
        }


        if (NetworkConnectivityClass.isNetworkAvailable(AddSalesmanActivity.this)) {

            ProgressDialogClass.showRoundProgress(AddSalesmanActivity.this,"Loading...");

            Log.d("ADDCOasY","name: "+ addsname +"\nemail: "+addsemail+"\npass"+addspass);

            //***********post user list********

            AndroidNetworking.upload(ConfigURL.URL_ADD_SALESMAN)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(AddSalesmanActivity.this))
                    .addMultipartParameter("name", addsname)
                    .addMultipartParameter("email", addsemail)
                    .addMultipartParameter("password", addspass)

                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(AddSalesmanActivity.this, "Added", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(AddSalesmanActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(AddSalesmanActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }
    }
}
