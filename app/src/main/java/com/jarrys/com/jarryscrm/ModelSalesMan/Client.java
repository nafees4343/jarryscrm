package com.jarrys.com.jarryscrm.ModelSalesMan;

public class Client {
    public String clientid,clientbname,clientname,clientphoneno,clientfax,clientemail;

    public Client(String clientid, String clientbname, String clientname, String clientphoneno, String clientfax, String clientemail) {
        this.clientid = clientid;
        this.clientbname = clientbname;
        this.clientname = clientname;
        this.clientphoneno = clientphoneno;
        this.clientfax = clientfax;
        this.clientemail = clientemail;
    }

    public String getClientbname() {
        return clientbname;
    }

    public void setClientbname(String clientbname) {
        this.clientbname = clientbname;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public String getClientphoneno() {
        return clientphoneno;
    }

    public void setClientphoneno(String clientphoneno) {
        this.clientphoneno = clientphoneno;
    }

    public String getClientfax() {
        return clientfax;
    }

    public void setClientfax(String clientfax) {
        this.clientfax = clientfax;
    }

    public String getClientemail() {
        return clientemail;
    }

    public void setClientemail(String clientemail) {
        this.clientemail = clientemail;
    }
}
