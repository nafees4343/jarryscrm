package com.jarrys.com.jarryscrm.AdaptersAdmin;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.Admin.UpdateProductActivity;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyProducts;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdapterProductsListRv extends RecyclerView.Adapter<AdapterProductsListRv.MyViewHolder> {

    private ArrayList<CompanyProducts> arrayList ;
    Context acontext;
    public MyAdapterListener onClickListener;

    public interface MyAdapterListener {
        void iconTextViewOnClick(View v, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tv_pname, tv_pquantity, tv_pprice;
        public ImageView iv_editproduct,iv_deleteproduct;
        ImageView ivproduct;


        public MyViewHolder(View v){
            super(v);

            //mCardView = (CardView) v.findViewById(R.id.cv_chat_list);
            tv_pname= (TextView) v.findViewById(R.id.tv_pname);
            tv_pquantity = (TextView) v.findViewById(R.id.tv_pquantity);
            tv_pprice = (TextView) v.findViewById(R.id.tv_pprice);
            //ivproduct = v.findViewById(R.id.ivproduct);
            iv_editproduct = v.findViewById(R.id.iv_editproduct);
            iv_deleteproduct = v.findViewById(R.id.iv_deleteproduct);
            ivproduct =  v.findViewById(R.id.ivproduct);

            //itemView.setOnClickListener(this); // bind the listener
            iv_editproduct.setOnClickListener(this);
            iv_deleteproduct.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            final CompanyProducts c = arrayList.get(getAdapterPosition());
            switch (v.getId()){
                case R.id.iv_editproduct:
                   Intent i = new Intent(acontext,UpdateProductActivity.class);
                   i.putExtra("pid",c.getId());
                   i.putExtra("productname",c.getName());
                   i.putExtra("productquantity",c.getQuantity());
                   i.putExtra("productprice",c.getUnit_price());
                   i.putExtra("productpicture",c.getPicture());
                    acontext.startActivity(i);
                    onClickListener.iconTextViewOnClick(v, getAdapterPosition());
                    Log.d("adadadasda","click hua");
                    break;
                case R.id.iv_deleteproduct:
                    Log.d("ididididid","pid"+c.getId());
                    open(v,c.getId(),getAdapterPosition());
                    break;
            }

/*
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
*/
        }
    }

    public void open(View view, final String getpid, final int adapterposition){
        AlertDialog alertDialog = null;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(acontext);
        alertDialogBuilder.setMessage("Are you sure, You wanted to delete product");
        alertDialogBuilder.setPositiveButton("yes",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        cancelRequest(getpid,adapterposition);
                    }
                });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
         alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void cancelRequest(String pid, final int position){
        ProgressDialogClass.showRoundProgress(acontext,"Loading...");
        AndroidNetworking.get(ConfigURL.URL_DELETE_PRODUCTS+pid)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(acontext))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("resres",""+response);
                        try {
                            ProgressDialogClass.dismissRoundProgress();
                            if (response.getBoolean("success")) {

                                Toast.makeText(acontext, "deleted", Toast.LENGTH_LONG).show();
                                removeAt(position);

                            } else if (!response.getBoolean("success")) {
                                Toast.makeText(acontext, "some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(acontext, "asd" + error, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void removeAt(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }

    public AdapterProductsListRv(Context context, ArrayList<CompanyProducts> arrayList , MyAdapterListener listener) {
        this.arrayList = arrayList;
        acontext = context;
        onClickListener = listener;

    }
    @Override
    public AdapterProductsListRv.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carditemproductlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){

        CompanyProducts current = arrayList.get(position);
        Log.d("DataDataData"," name : "+ current.getName());
        holder.tv_pname.setText("name: "+current.getName());
        holder.tv_pquantity.setText("qty: "+current.getQuantity());
        holder.tv_pprice.setText("price: "+current.getUnit_price());

       if (current.getPicture() != null) {

                Picasso.get()
                .load(current.getPicture())
                .fit()
                .centerCrop()
                .into(holder.ivproduct);


        } else {
            holder.ivproduct.setImageResource(R.drawable.imgcompanydefualt);
        }


    }

    @Override
    public int getItemCount() { return arrayList.size(); }

}
