package com.jarrys.com.jarryscrm.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomButton extends android.support.v7.widget.AppCompatButton {
    public CustomButton(Context context) {
        super(context);
        setFont();
    }
    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }
    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/Montserrat_Regular.ttf");
        setTypeface(font, Typeface.NORMAL);
    }
}