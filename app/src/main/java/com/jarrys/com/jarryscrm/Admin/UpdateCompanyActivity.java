package com.jarrys.com.jarryscrm.Admin;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class UpdateCompanyActivity extends AppCompatActivity implements View.OnClickListener,IPickResult {

    String cid;
    Button btn_updatecadd,btn_updateccancel;
    EditText et_updatecname,et_updatecdescription,et_updatecphoneno,et_updatecemail,et_updatecaddress;
    String updatecname,updatecdescription,updatecphoneno,updatecemail,updatecaddress,imgcompany;
    ImageView iv_updatecompanyl,iv;
    Bitmap Bmap;
    ByteArrayOutputStream bStream;
    byte[] byteArray;
    File f1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_company);


        if( getIntent().getExtras() != null)
        {
            Intent i = getIntent();
            cid = i.getStringExtra("companyid");
            updatecname = i.getStringExtra("companyname");
            updatecaddress = i.getStringExtra("companyaddress");
            updatecdescription = i.getStringExtra("companydescription");
            updatecemail = i.getStringExtra("companyemail");
            updatecphoneno = i.getStringExtra("companyphoneno");
            imgcompany = i.getStringExtra("companyimage");

        }

        Log.d("companyidcompanyid","pid; "+cid+"\nCname: "+updatecname+"\nCaddress: "+updatecaddress+"\nCdescription: "+updatecdescription
                +"\nupdatecemail: "+updatecemail+"\nupdatecphoneno: "+updatecphoneno);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        et_updatecname = findViewById(R.id.et_updatecname);
        et_updatecdescription = findViewById(R.id.et_updatecdescription);
        et_updatecphoneno = findViewById(R.id.et_updatecphoneno);
        et_updatecemail = findViewById(R.id.et_updatecemail);
        et_updatecaddress = findViewById(R.id.et_updatecaddress);
        iv_updatecompanyl = findViewById(R.id.iv_updatecpicture);

        btn_updatecadd = findViewById(R.id.btn_updatecadd);
        btn_updateccancel = findViewById(R.id.btn_updateccancel);

        et_updatecname.setText(updatecname);
        et_updatecaddress.setText(updatecaddress);
        et_updatecdescription.setText(updatecdescription);
        et_updatecemail.setText(updatecemail);
        et_updatecphoneno.setText(updatecphoneno);
        if (imgcompany != null) {

            Picasso.get()
                    .load(imgcompany)
                    .into(new Target() {

                        @Override
                        public void onBitmapLoaded (final Bitmap bitmap, Picasso.LoadedFrom from) {
                            /* Save the bitmap or do something with it here */

                            Bmap = bitmap;

                            try {
                                f1 = returnFile(bitmap);
                                Log.d("cscscscsc","ayaya:"+Bmap);

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            // Set it in the ImageView
                            iv_updatecompanyl.setImageBitmap(Bmap);
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {}


                    });


        } else {
            iv_updatecompanyl.setImageResource(R.drawable.imgcompanydefualt);
        }





        btn_updatecadd.setOnClickListener(this);
        btn_updateccancel.setOnClickListener(this);
        iv_updatecompanyl.setOnClickListener(this);



    }



    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_updatecadd:
                senddata();
                break;
            case R.id.btn_updateccancel:
                finish();
                break;
            case R.id.iv_updatecpicture:
                PickImageDialog.build(new PickSetup()).show(this);
                break;
        }
    }



    public File returnFile(Bitmap bmp) throws IOException {
        File f = new File(getCacheDir(), ".png");
        f.createNewFile();
        //Convert bitmap to byte array
        Bitmap bitmap = bmp;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }


    public void senddata(){

        updatecname = et_updatecname.getText().toString();
        updatecdescription = et_updatecdescription.getText().toString();
        updatecphoneno = et_updatecphoneno.getText().toString();
        updatecemail = et_updatecemail.getText().toString();
        updatecaddress = et_updatecaddress.getText().toString();


        if(updatecname.equals("")){
            Toast.makeText(UpdateCompanyActivity.this, "company name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(updatecdescription.equals("")){
            Toast.makeText(UpdateCompanyActivity.this, "description is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(updatecphoneno.equals("")){
            Toast.makeText(UpdateCompanyActivity.this, "phone no is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(updatecemail.equals("")){
            Toast.makeText(UpdateCompanyActivity.this, "email is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(updatecaddress.equals("")){
            Toast.makeText(UpdateCompanyActivity.this, "address name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(Bmap==null){
            Toast.makeText(UpdateCompanyActivity.this, "Image is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(UpdateCompanyActivity.this)) {

            ProgressDialogClass.showRoundProgress(UpdateCompanyActivity.this,"Loading...");

            Log.d("ADDCOMPANYY","pid: "+cid+"ADDCNAME: "+updatecname+"\nAddCDESCRIPTION: "+updatecdescription+"\nAddCPhone"+updatecphoneno+"\nAddress"+updatecaddress+updatecemail+"\nfile: "+f1);


            //***********post user list********

            AndroidNetworking.upload(ConfigURL.URL_UPDATE_COMPANIES)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(UpdateCompanyActivity.this))
                    .addMultipartParameter("company_id", cid)
                    .addMultipartParameter("company_name", updatecname)
                    .addMultipartParameter("company_description", updatecdescription)
                    .addMultipartParameter("company_phone", updatecphoneno)
                    .addMultipartParameter("company_address", updatecaddress)
                    .addMultipartParameter("company_email", updatecemail)
                    .addMultipartFile("company_image", f1)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(UpdateCompanyActivity.this, "Updated", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(UpdateCompanyActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(UpdateCompanyActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            Bmap = r.getBitmap();

            try {
                f1 = returnFile(Bmap);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //If you want the Bitmap.
            iv_updatecompanyl.setImageBitmap(r.getBitmap());

            //Image path
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
