package com.jarrys.com.jarryscrm.Salesman;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.Admin.AddCompanyActivity;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONException;
import org.json.JSONObject;


public class AddClientActivity extends AppCompatActivity {

    String cid,clientname,clientfname,clientlname,clientphoneno,clientfax,clientemail;
    EditText et_clientbname,et_clientfname,et_clientlname,et_clientphoneno,et_clientfax,et_clientemail;
    Button btn_addclient,btn_addclientcancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_client);

        if(getIntent().getExtras()!=null){
            cid = getIntent().getExtras().getString("cid");
            Log.d("cccccid",""+cid);
        }

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        InitViews();

        btn_addclient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                senddata();
            }
        });

        btn_addclientcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void senddata(){

        clientname = et_clientbname.getText().toString();
        clientfname = et_clientfname.getText().toString();
        clientlname = et_clientlname.getText().toString();
        clientphoneno = et_clientphoneno.getText().toString();
        clientfax = et_clientfax.getText().toString();
        clientemail = et_clientemail.getText().toString();


        if(clientname.equals("")){
            Toast.makeText(AddClientActivity.this, "name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(clientfname.equals("")){
            Toast.makeText(AddClientActivity.this, "first name is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(clientlname.equals("")){
            Toast.makeText(AddClientActivity.this, "last name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(clientphoneno.equals("")){
            Toast.makeText(AddClientActivity.this, "phone no is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(clientfax.equals("")){
            Toast.makeText(AddClientActivity.this, "fax is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(clientemail.equals("")){
            Toast.makeText(AddClientActivity.this, "email is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(AddClientActivity.this)) {

            ProgressDialogClass.showRoundProgress(AddClientActivity.this,"Loading...");

            Log.d("ADDCOMPANYY","clientname: "+clientname+"\nclientfname: "+clientfname+"\nclientlname"+clientlname+"\nclientphoneno"+clientphoneno+"\nfax:"+clientfax+"\nemail: "+clientemail);

            //***********post user list********

            AndroidNetworking.post(ConfigURL.URL_ADD_CLIENTS_SALESMAN)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(AddClientActivity.this))
                    .addBodyParameter("business_name", clientname)
                    .addBodyParameter("client_first_name", clientfname)
                    .addBodyParameter("client_last_name", clientlname)
                    .addBodyParameter("phone_number", clientphoneno)
                    .addBodyParameter("fax", clientfax)
                    .addBodyParameter("email_address", clientemail)
                    .addBodyParameter("company_id", cid)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(AddClientActivity.this, "Added", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(AddClientActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(AddClientActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }




    }

    public void InitViews(){

        et_clientbname = findViewById(R.id.et_bnameaddclient);
        et_clientfname = findViewById(R.id.et_firstnameaddclient);
        et_clientlname = findViewById(R.id.et_lastnameaddclient);
        et_clientphoneno = findViewById(R.id.et_phonenoaddclient);
        et_clientfax = findViewById(R.id.et_faxaddclient);
        et_clientemail = findViewById(R.id.et_emailaddclient);
        btn_addclient = findViewById(R.id.btn_addclient);
        btn_addclientcancel = findViewById(R.id.btn_canceladdclient);
    }
}
