package com.jarrys.com.jarryscrm.AdaptersSalesman;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jarrys.com.jarryscrm.ModelSalesMan.Client;
import com.jarrys.com.jarryscrm.R;

import java.util.ArrayList;

public class AdapterClientListRv extends RecyclerView.Adapter<AdapterClientListRv.MyViewHolder> {

    private ArrayList<Client> arrayList ;
    Context acontext;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_nameclient, tv_businessnameclient, tv_phoneno,tv_emailclient;


        public MyViewHolder(View v){
            super(v);

            //mCardView = (CardView) v.findViewById(R.id.cv_chat_list);
            tv_nameclient = (TextView) v.findViewById(R.id.tv_nameclient);
            tv_businessnameclient = (TextView) v.findViewById(R.id.tv_businessnameclient);
            tv_emailclient = v.findViewById(R.id.tv_emailclient);

            //itemView.setOnClickListener(this); // bind the listener

        }

    }


    public AdapterClientListRv(Context context, ArrayList<Client> arrayList) {
        this.arrayList = arrayList;
        acontext = context;

    }
    @Override
    public AdapterClientListRv.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.carditemclientlist, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position){

        Client current = arrayList.get(position);
        Log.d("DataDataDataDataData"," name : " + current.getClientname());
        holder.tv_nameclient.setText(current.getClientname());
        holder.tv_businessnameclient.setText(current.getClientbname());
        holder.tv_emailclient.setText(current.getClientemail());


    }

    @Override
    public int getItemCount() { return arrayList.size(); }

}
