package com.jarrys.com.jarryscrm.Admin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.AdaptersAdmin.AdapterCompaniesListRv;
import com.jarrys.com.jarryscrm.AdaptersAdmin.AdapterSalesmanListRv;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.ModelAdmin.SalesmanDetails;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


public class ViewSalesmanFragment extends Fragment implements View.OnClickListener{

    ArrayList<SalesmanDetails> list;
    AdapterSalesmanListRv adapter;
    RecyclerView rv;
    Button btn_viewsalesmanaddcompany;
    boolean chk = true;

    public ViewSalesmanFragment() {
        // Required empty public constructor
    }

    public MyAdapterListener onClickListener;

    public interface MyAdapterListener {

        void iconTextViewOnClick(View v, int position);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_salesman, container, false);
        rv = rootView.findViewById(R.id.rvsalesmanlist);

        chk = false;

        btn_viewsalesmanaddcompany = rootView.findViewById(R.id.btn_viewsalesmanaddcompany);


        get_data();


        btn_viewsalesmanaddcompany.setOnClickListener(this);


        return rootView;
    }

    /**
     * Called when the fragment is visible to the user and actively running.
     * This is generally
     * tied to {@linkActivity#onResume() Activity.onResume} of the containing
     * Activity's lifecycle.
     */
    @Override
    public void onResume() {
        super.onResume();

        if(chk) {
            Log.d("VVISBILE","agae "+chk);
            list.clear();
            get_data();
        }

    }



    public void get_data() {
        ProgressDialogClass.showRoundProgress(getActivity(),"Loading...");
        Log.d("headerheader","header: "+ConfigURL.gettoken(getActivity()));
        AndroidNetworking.get(ConfigURL.URL_GET_SALESMAN)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissRoundProgress();
                        Log.d("VSresponse","res:"+response);
                        boolean status = false;

                        //String img_tag = "http://goodwisesearch.com/uploads/";

                        try {
                            JSONArray jsonArray = response.getJSONArray("salesman");
                            Log.d("asddaad",""+jsonArray.length());
                            list = new ArrayList<>();
                            for(int i=0; i<jsonArray.length(); i++) {
                                JSONObject data = jsonArray.getJSONObject(i);
                                String id = String.valueOf(data.getInt("id"));
                                String name = (String) data.get("name");
                                String email = (String) data.get("email");

                                Log.d("asdadsfsdas","id: "+id+"\nName: "+name+"\nemail: "+email);

                                SalesmanDetails companyDetails = new SalesmanDetails(id,name,email);
                                list.add(companyDetails);
                            }

                            adapter = new AdapterSalesmanListRv(getActivity(),list,new AdapterSalesmanListRv.MyAdapterListener() {
                                @Override
                                public void iconTextViewOnClick(View v, int position) {
                                    Log.d("chkclick", "iconTextViewOnClick at position "+position);
                                    chk = true;
                                }
                            });

                            rv.setAdapter(adapter);
                            rv.setHasFixedSize(true);
                            rv.setItemViewCacheSize(20);
                            rv.setDrawingCacheEnabled(true);
                            rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                            rv.setLayoutManager(llm);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(ViewActivity.this, "code:" , Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(getActivity(), "Some thing went wrong" , Toast.LENGTH_LONG).show();

                    }
                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_viewsalesmanaddcompany:
                Intent i = new Intent(getActivity(),AddSalesmanActivity.class);
                startActivity(i);
                chk = true;
                break;
        }
    }
}
