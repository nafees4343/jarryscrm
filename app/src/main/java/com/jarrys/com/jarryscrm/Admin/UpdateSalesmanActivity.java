package com.jarrys.com.jarryscrm.Admin;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class UpdateSalesmanActivity extends AppCompatActivity implements View.OnClickListener {

    String sid;
    Button btn_updatesupdate, btn_updatescancel,btn_changepass_salesman;
    EditText et_updatesname,et_updatecdescription,et_updatecphoneno,et_updatecemail, et_updatesemail;
    String updatesname,updatecdescription,updatecphoneno,updatecemail, updatesemail,imgcompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_salesman);


        if( getIntent().getExtras() != null)
        {
            Intent i = getIntent();
            sid = i.getStringExtra("salesmanid");
            updatesname = i.getStringExtra("salesmanname");
            updatesemail = i.getStringExtra("salesmanemail");
        }

        Log.d("companyidcompanyid","Sid; "+sid+"\nSname: "+ updatesname +"\nSaddress: "+ updatesemail);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        et_updatesname = findViewById(R.id.et_updatesname);
        et_updatesemail = findViewById(R.id.et_updatesemail);

        btn_updatesupdate = findViewById(R.id.btn_updatesupdate);
        btn_updatescancel = findViewById(R.id.btn_updatescancel);

        btn_changepass_salesman = findViewById(R.id.btn_changepass_salesman);


        et_updatesname.setText(updatesname);
        et_updatesemail.setText(updatesemail);

        btn_updatesupdate.setOnClickListener(this);
        btn_updatescancel.setOnClickListener(this);
        btn_changepass_salesman.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_updatesupdate:
                senddata();
                break;
            case R.id.btn_updatescancel:
                finish();
                break;
            case R.id.btn_changepass_salesman:
                Intent i = new Intent(UpdateSalesmanActivity.this,ChangePassSalesmanActivity.class);
                i.putExtra("salesmanid",sid);
                startActivity(i);
                break;
        }
    }

    public void senddata(){

        updatesname = et_updatesname.getText().toString();
        updatesemail = et_updatesemail.getText().toString();


        if(updatesname.equals("")){
            Toast.makeText(UpdateSalesmanActivity.this, "name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(updatesemail.equals("")){
            Toast.makeText(UpdateSalesmanActivity.this, "email is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(UpdateSalesmanActivity.this)) {

            ProgressDialogClass.showRoundProgress(UpdateSalesmanActivity.this,"Loading...");

            Log.d("ADDCOMPANYY","pid: "+sid+"ADDsNAME: "+updatesname+"\nsEMAIL: "+updatesemail);


            //***********post user list********

            AndroidNetworking.post(ConfigURL.URL_UPDATE_SALESMAN)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(UpdateSalesmanActivity.this))
                    .addBodyParameter("id", sid)
                    .addBodyParameter("name", updatesname)
                    .addBodyParameter("email", updatesemail)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(UpdateSalesmanActivity.this, "Updated", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(UpdateSalesmanActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            //Toast.makeText(AddActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(UpdateSalesmanActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }
    }
}
