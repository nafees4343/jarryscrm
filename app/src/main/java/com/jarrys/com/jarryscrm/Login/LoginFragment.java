package com.jarrys.com.jarryscrm.Login;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.Drawer;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.CustomButton;
import com.jarrys.com.jarryscrm.utils.CustomTextView;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    CustomTextView tv_email,tv_password,tv_forgotpassword;
    CustomButton btn_login;
    EditText et_email,et_password;
    String email,pass;
    String LOGIN = "active";


    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);


        if (ConfigURL.getLoginState(getActivity()).length() > 0) {
            Intent intent = new Intent(getActivity(), Drawer.class);
            startActivity(intent);
            getActivity().finish();
        }

        tv_email = rootView.findViewById(R.id.tv_emaill);
        tv_password = rootView.findViewById(R.id.tv_password);
        tv_forgotpassword = rootView.findViewById(R.id.tv_forgotpassword);

        et_email = rootView.findViewById(R.id.et_emaillogin);
        et_password = rootView.findViewById(R.id.et_passwordlogin);
        btn_login = rootView.findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
        return rootView;
    }



    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public void sendData() {

        ProgressDialogClass.showRoundProgress(getActivity(),"Loading...");

        email = et_email.getText().toString();
        pass = et_password.getText().toString();

        //        Toast.makeText(this, "" + email + "" + pass, Toast.LENGTH_LONG).show();

        AndroidNetworking.post(ConfigURL.URL_LOGIN_PERSON)
                .addBodyParameter("email", email)
                .addBodyParameter("password", pass)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissRoundProgress();
                        Log.d("SignIn", "res:" + response);
                        try {
                            String token;
                            boolean error = false;
                            error = response.getBoolean("success");
                            if (error) {
                                token = response.getString("token");
                                JSONArray jsonObject = response.getJSONArray("user");
                                int id = jsonObject.getJSONObject(0).getInt("id");
                                String name = jsonObject.getJSONObject(0).getString("name");
                                String email = jsonObject.getJSONObject(0).getString("email");
                                int role_id = jsonObject.getJSONObject(0).getInt("role_id");

                                SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putString("LOGIN", LOGIN);
                                editor.putString("name", name);
                                editor.putString("email", email);
                                editor.putString("role_id", ( String.valueOf(role_id)));
                                editor.putString("token",token);
                                editor.commit();


                                Bundle bundle = new Bundle();
                                Intent intent = new Intent(getActivity(), Drawer.class);
                                bundle.putInt("role_id", role_id);
                                intent.putExtras(bundle);
                                startActivity(intent);
                                getActivity().finish();



                            }
                            else if(!error){
                                String msg;
                                msg = response.getString("message");
                                Toast.makeText(getActivity(), "" + msg, Toast.LENGTH_LONG).show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("SignIn", "res:" + e);

                        }

                    }

                    @Override
                    public void onError(ANError error) {
                        ProgressDialogClass.dismissRoundProgress();
                        Log.d("SignIn", "" + error);
                        Toast.makeText(getActivity(), "Some thing went wrong", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void submit() {

        if (et_email.getText().toString().isEmpty()) {
            et_email.setError("email cannot be empty");
            requestFocus(et_email);
            return;
        }

        if(!isValidEmail(et_email.getText().toString()))
        {
            et_email.setError("Invalid email");
            requestFocus(et_email);
            return;
        }

        if (et_password.getText().toString().isEmpty()) {
            et_password.setError("password cannot be empty");
            requestFocus(et_password);
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(getActivity())) {
            sendData();
        } else {
            Toast.makeText(getActivity(), "Internet Not Connected", Toast.LENGTH_LONG).show();
        }
    }



    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.content_frame_dashboard, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

   // 3.16.51.233/api/auth/login

}
