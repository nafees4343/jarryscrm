package com.jarrys.com.jarryscrm.ModelAdmin;

public class CompanyProducts {
    public String id;
    public String name;
    public String picture;
    public String unit_price;
    public String quantity;
    public Integer company_id;

    public CompanyProducts(String id, String name, String picture, String unit_price, String quantity, Integer company_id) {
        this.id = id;
        this.name = name;
        this.picture = picture;
        this.unit_price = unit_price;
        this.quantity = quantity;
        this.company_id = company_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Integer getCompany_id() {
        return company_id;
    }

    public void setCompany_id(Integer company_id) {
        this.company_id = company_id;
    }
}
