package com.jarrys.com.jarryscrm.AdaptersSalesman;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jarrys.com.jarryscrm.Salesman.BrandMenuFragment;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AdapterBrandsRv extends RecyclerView.Adapter<AdapterBrandsRv.MyViewHolder> {

    private ArrayList<CompanyDetails> arrayList ;
    Context acontext;
    public AdapterBrandsRv.MyAdapterListener onClickListener;
    BrandMenuFragment brandMenuFragment = null;
    public interface MyAdapterListener {
        void iconTextViewOnClick(View v, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tv_name, tv_email, tv_phoneno;
        public ImageView iveditcompany,ivdeletecompany;
        ImageView ivcompany ;


        public MyViewHolder(View v){
            super(v);

            ivcompany =  v.findViewById(R.id.img_brand);

            //itemView.setOnClickListener(this); // bind the listener
            ivcompany.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            final CompanyDetails c = arrayList.get(getAdapterPosition());
            switch (v.getId()){
                case R.id.img_brand:
                    Bundle bundle = new Bundle();
                     brandMenuFragment = new BrandMenuFragment();
                     bundle.putString("cid",""+c.getcid());
                     brandMenuFragment.setArguments(bundle);
                    replaceFragment(brandMenuFragment);
                    onClickListener.iconTextViewOnClick(v, getAdapterPosition());
                    Log.d("adadadasda","click hua");
                    break;
            }

/*
            if (clickListener != null) clickListener.onClick(v, getAdapterPosition());
*/
        }
    }


    public AdapterBrandsRv(Context context, ArrayList<CompanyDetails> arrayList , AdapterBrandsRv.MyAdapterListener listener) {
        this.arrayList = arrayList;
        acontext = context;
        onClickListener = listener;

    }
    @Override
    public AdapterBrandsRv.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_brands, parent, false);
        AdapterBrandsRv.MyViewHolder vh = new AdapterBrandsRv.MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterBrandsRv.MyViewHolder holder, int position) {
        CompanyDetails current = arrayList.get(position);
        Log.d("DataDataData"," url : "+ current.getCompany_image());

        if (current.getCompany_image() != null) {

            Picasso.get()
                    .load(current.getCompany_image())
                    .fit()
                    .into(holder.ivcompany);

        } else {
            holder.ivcompany.setImageResource(R.drawable.imgcompanydefualt);
        }
    }


    @Override
    public int getItemCount() { return arrayList.size(); }

    private void replaceFragment(android.support.v4.app.Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = ((FragmentActivity)acontext).getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
