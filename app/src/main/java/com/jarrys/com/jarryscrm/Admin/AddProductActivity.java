package com.jarrys.com.jarryscrm.Admin;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.AdaptersAdmin.CustomListAdapterDialog;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.NetworkConnectivityClass;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;
import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AddProductActivity extends AppCompatActivity implements View.OnClickListener ,Serializable,IPickResult {

    Dialog dialog;
    List<CompanyDetails> listOfCountry;
    CustomListAdapterDialog clad;
    ListView lvcompanies;
    ArrayList<CompanyDetails> myList;
    Button btn_selectcompany,btn_addpadd,btn_addpcancel;
    EditText et_addpname, et_addpquantity, et_addpprice;
    String addpname,addpquantity,addpprice,addpcompany,iv_company;
    ImageView iv_addppicture;
    Bitmap bitmap;
    File f1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
        );

        btn_selectcompany = findViewById(R.id.btn_selectcompany);

        et_addpname = findViewById(R.id.et_addpname);
        et_addpquantity = findViewById(R.id.et_addpquantity);
        et_addpprice = findViewById(R.id.et_addpprice);

        iv_addppicture = findViewById(R.id.iv_addppicture);
        btn_addpadd = findViewById(R.id.btn_addpadd);
        btn_addpcancel = findViewById(R.id.btn_addpcancel);

        if( getIntent().getExtras() != null)
        {
            //do here
             myList = (ArrayList<CompanyDetails>) getIntent().getSerializableExtra("companylist");
        }

        btn_selectcompany.setOnClickListener(this);
        iv_addppicture.setOnClickListener(this);
        btn_addpadd.setOnClickListener(this);
        btn_addpcancel.setOnClickListener(this);
    }

    public void senddata(){

        addpname = et_addpname.getText().toString();
        addpquantity = et_addpquantity.getText().toString();
        addpprice = et_addpprice.getText().toString();


        Log.d("sadasdasd","addpname:"+addpname+"\naddpquantity:"+addpquantity+"\naddpprice:"+addpprice+"\naddpcompany:"+addpcompany+"\nbitmap:"+bitmap);

        if(addpname.equals("")){
            Toast.makeText(AddProductActivity.this, "product name is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(addpquantity.equals("")){
            Toast.makeText(AddProductActivity.this, "quantity is missing", Toast.LENGTH_SHORT).show();
            return;
        }
        if(addpprice.equals("")){
            Toast.makeText(AddProductActivity.this, "price is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(addpcompany.equals("")){
            Toast.makeText(AddProductActivity.this, "company is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if(bitmap==null){
            Toast.makeText(AddProductActivity.this, "Image is missing", Toast.LENGTH_SHORT).show();
            return;
        }

        if (NetworkConnectivityClass.isNetworkAvailable(AddProductActivity.this)) {

            ProgressDialogClass.showRoundProgress(AddProductActivity.this,"Loading...");

            Log.d("ADDCOMPANYY","ADDPNAME: "+addpname+"\nAddPDESCRIPTION: "+addpquantity+"\nAddPPhone"+addpprice+"\nCompany"+addpcompany+"\nfile: "+f1+"\nToken:"+ConfigURL.gettoken(AddProductActivity.this));

            //***********post user list********

            AndroidNetworking.upload(ConfigURL.URL_ADD_PRODUCTS)
                    .addHeaders("Authorization","bearer "+ConfigURL.gettoken(AddProductActivity.this))
                    .addMultipartParameter("name", addpname)
                    .addMultipartParameter("quantity", addpquantity)
                    .addMultipartParameter("unit_price", addpprice)
                    .addMultipartParameter("company_id", addpcompany)
                    .addMultipartFile("picture", f1)
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("asdasdasdasdas","rr;"+response);
                            ProgressDialogClass.dismissRoundProgress();
                            boolean success = false;
                            try {
                                success = response.getBoolean("success");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if (success) {
                                Toast.makeText(AddProductActivity.this, "Added", Toast.LENGTH_LONG).show();
                                finish();
                            }
                            else {
                                Toast.makeText(AddProductActivity.this, "Some thing went wrong", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            Log.v("SignIn", "" + error);
                            ProgressDialogClass.dismissRoundProgress();
                            Toast.makeText(AddProductActivity.this, "On error", Toast.LENGTH_LONG).show();
                        }
                    });



        } else {
            Toast.makeText(AddProductActivity.this, "Internet Not Connected", Toast.LENGTH_LONG).show();
        }




    }


    public File returnFile(Bitmap bmp) throws IOException {
        File f = new File(getCacheDir(), ".png");
        f.createNewFile();
        //Convert bitmap to byte array
        Bitmap bitmap = bmp;
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
        byte[] bitmapdata = bos.toByteArray();
        //write the bytes in file
        FileOutputStream fos = new FileOutputStream(f);
        fos.write(bitmapdata);
        fos.flush();
        fos.close();
        return f;
    }



    private void showDialogCountries(){

        dialog = new Dialog(this);

        View view = getLayoutInflater().inflate(R.layout.dialog_main, null);

        lvcompanies = (ListView) view.findViewById(R.id.custom_listofcompanies);

        myList.removeAll(Arrays.asList(null,""));


        // Change MyActivity.this and myListOfItems to your own values
        clad = new CustomListAdapterDialog(AddProductActivity.this,  myList);

        lvcompanies.setAdapter(clad);

        dialog.setCancelable(true);

        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_selectcompany:
                showDialogCountries();
                lvcompanies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Log.d("listOfCountry","Selected: "+clad.getItem(i));
                        CompanyDetails c = myList.get(i);
                        btn_selectcompany.setText(""+c.getCompany_name());
                        addpcompany = c.getcid();
                        dialog.cancel();
                    }
                });
                break;
            case R.id.btn_addpadd:
                senddata();
                break;
            case R.id.btn_addpcancel:
                finish();
                break;
            case R.id.iv_addppicture:
                PickImageDialog.build(new PickSetup()).show(this);
                break;
        }
    }

    @Override
    public void onPickResult(PickResult r) {
        if (r.getError() == null) {
            //If you want the Uri.
            //Mandatory to refresh image from Uri.
            //getImageView().setImageURI(null);

            //Setting the real returned image.
            //getImageView().setImageURI(r.getUri());

            bitmap = r.getBitmap();


            try {
                f1 = returnFile(r.getBitmap());
            } catch (IOException e) {
                e.printStackTrace();
            }

            //If you want the Bitmap.
            iv_addppicture.setImageBitmap(r.getBitmap());

            //Image path
            //r.getPath();
        } else {
            //Handle possible errors
            //TODO: do what you have to do with r.getError();
            //Toast.makeText(this, r.getError().getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
