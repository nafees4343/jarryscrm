package com.jarrys.com.jarryscrm.Salesman;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.AdaptersSalesman.AdapterBrandsRv;
import com.jarrys.com.jarryscrm.Admin.ViewCompanyFragment;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class BrandsFragment extends Fragment {

    private static RecyclerView recyclerView;
    AdapterBrandsRv adapter;
    ArrayList<CompanyDetails> list;
    RecyclerView rv;
    Button btn_viewcompanyadd;
    boolean chk = true;


    public BrandsFragment() {
        // Required empty public constructor
    }

    public ViewCompanyFragment.MyAdapterListener onClickListener;

    public interface MyAdapterListener {

        void iconTextViewOnClick(View v, int position);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_brands, container, false);
        initViews(rootView);

        chk = false;

        get_data();

        return rootView;
    }



    // Initialize the view
    private void initViews(View view) {

         rv = (RecyclerView)
                view.findViewById(R.id.recycler_view);

    }

    public void get_data() {
        ProgressDialogClass.showRoundProgress(getActivity(),"Loading...");
        Log.d("headerheader","header: "+ConfigURL.gettoken(getActivity()));
        AndroidNetworking.get(ConfigURL.URL_GET_COMPANIES_SALESMAN)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissRoundProgress();
                        Log.d("VSCresponse","res:"+response);
                        boolean status = false;

                        //String img_tag = "http://goodwisesearch.com/uploads/";

                        try {
                            JSONArray jsonArray = response.getJSONArray("companies");
                            Log.d("asddaad",""+jsonArray.length());
                            list = new ArrayList<>();
                            for(int i=0; i<jsonArray.length(); i++) {
                                JSONObject data = jsonArray.getJSONObject(i);
                                String id = String.valueOf(data.getInt("id"));
                                String company_name = (String) data.get("company_name");
                                String company_description = (String) data.get("company_description");
                                String company_image = ConfigURL.ip+(String) data.get("company_image");
                                String company_phone = (String) data.get("company_phone");
                                String company_address = (String) data.get("company_address");
                                String company_email = (String) data.get("company_email");
                                String user_id = String.valueOf(data.get("user_id"));;

                                Log.d("asdadas","id:"+id+"\nName: "+company_name+"\nDescription: "+company_description+"\nImage: "+company_image+"\nPhone: "+company_phone+"\nAddress: "+company_address+"\ncemail: "+company_email+"\nUserId: "+user_id);

                                CompanyDetails companyDetails = new CompanyDetails(id,company_name,company_description,company_image,company_phone,company_address,company_email,user_id);
                                list.add(companyDetails);
                            }

                            adapter = new AdapterBrandsRv(getActivity(),list,new AdapterBrandsRv.MyAdapterListener() {
                                @Override
                                public void iconTextViewOnClick(View v, int position) {
                                    Log.d(TAG, "iconTextViewOnClick at position "+position);
                                    chk = true;
                                }
                            });

                            rv.setAdapter(adapter);
                            rv.setItemViewCacheSize(20);
                            rv.setDrawingCacheEnabled(true);
                            rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                            rv.setHasFixedSize(true);
                            //Set RecyclerView type according to intent value
                            rv.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(ViewActivity.this, "code:" , Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(getActivity(), "Some thing went wrong" , Toast.LENGTH_LONG).show();

                    }
                });
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                getActivity().finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
