package com.jarrys.com.jarryscrm.Salesman;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jarrys.com.jarryscrm.R;


public class BrandMenuFragment extends Fragment implements View.OnClickListener{

    LinearLayout l_addclient,l_viewclient;
    String cid;
    Bundle bundle;

    public BrandMenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_brand_menu, container, false);
        bundle = getArguments();
        if(bundle!=null) {
             cid = bundle.getString("cid");
        }

        Log.d("cidcidecide",""+cid);


        l_addclient = rootView.findViewById(R.id.l_addclient);
        l_viewclient = rootView.findViewById(R.id.l_client);
        l_addclient.setOnClickListener(this);
        l_viewclient.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View view) {
        Fragment fragmentName;
        switch (view.getId()){
            case R.id.l_addclient:
                Bundle b = new Bundle();
                Intent i = new Intent(getActivity(),AddClientActivity.class);
                b.putString("cid",cid);
                i.putExtras(b);
                startActivity(i);
                break;

            case R.id.l_client:
                fragmentName = new ViewClientsFragment();
                bundle.putString("cid",cid);
                fragmentName.setArguments(bundle);
                replaceFragment(fragmentName);
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getActivity().getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) { //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.fragment_container, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }
}
