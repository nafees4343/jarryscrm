package com.jarrys.com.jarryscrm.Salesman;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.AdaptersSalesman.AdapterClientListRv;
import com.jarrys.com.jarryscrm.ModelSalesMan.Client;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewClientsFragment extends Fragment {

    ArrayList<Client> list;
    AdapterClientListRv adapter;
    RecyclerView rv;
    Button btn_viewsalesmanaddcompany;
    boolean chk = true;
    TextView tv_nodata;
    Bundle bundle;
    String cid;

    public ViewClientsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_clients, container, false);

        bundle = getArguments();
        if(bundle!=null){
            cid = bundle.getString("cid");
            Log.d("cidcidecidasdae",""+cid);
        }

        chk = false;

        rv = rootView.findViewById(R.id.rvclientlist);

        tv_nodata = rootView.findViewById(R.id.tv_no_data_client);


        get_data();


        return rootView;
    }


    public void get_data() {
        ProgressDialogClass.showRoundProgress(getActivity(),"Loading...");
        Log.d("headerheader","header: "+ConfigURL.gettoken(getActivity()));
        AndroidNetworking.get(ConfigURL.URL_GET_CLIENTS_SALESMAN+cid)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissRoundProgress();
                        Log.d("VClientresponse","res:"+response);
                        boolean status = false;

                        //String img_tag = "http://goodwisesearch.com/uploads/";

                        try {
                            JSONArray jsonArray = response.getJSONArray("clients");
                            Log.d("asddaad",""+jsonArray.length());
                            list = new ArrayList<>();

                            if(jsonArray.length()>0) {


                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject data = jsonArray.getJSONObject(i);
                                    String clientid = String.valueOf(data.getInt("id"));
                                    String client_business_name = (String) data.get("business_name");
                                    String client_first_name = (String) data.get("client_first_name");
                                    String client_last_name =  (String) data.get("client_last_name");
                                    String client_phone = (String) data.get("phone_number");
                                    String client_fax = (String) data.get("fax");
                                    String client_email = (String) data.get("email_address");

                                    Log.d("asdadas", "clientid: " + clientid + "\nclient_business_name: " + client_business_name + "\nName: " + client_first_name + " " + client_last_name + "\nphone: " + client_phone + "\nfax: " + client_fax + "\nemail: " + client_email);

                                    Client client = new Client(clientid, client_business_name, client_first_name + " " + client_last_name, client_phone, client_fax, client_email);
                                    list.add(client);
                                }

                                adapter = new AdapterClientListRv(getActivity(), list);

                                rv.setAdapter(adapter);
                                rv.setHasFixedSize(true);
                                rv.setItemViewCacheSize(20);
                                rv.setDrawingCacheEnabled(true);
                                rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                rv.setLayoutManager(llm);

                            }
                            else {
                                Log.d("anannaa","aya:"+jsonArray.length());
                                list.clear();
                                rv.setVisibility(View.GONE);
                                tv_nodata.setVisibility(View.VISIBLE);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(ViewActivity.this, "code:" , Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(getActivity(), "Some thing went wrong" , Toast.LENGTH_LONG).show();

                    }
                });
    }
}
