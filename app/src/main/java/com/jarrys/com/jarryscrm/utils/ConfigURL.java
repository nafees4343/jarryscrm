package com.jarrys.com.jarryscrm.utils;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jarrys.com.jarryscrm.R;

/**
 * Created by Qasim Ahmed on 28/07/2018.
 */

public class ConfigURL {

    public static final String ip = "http://3.16.51.233";
    public static final String URL_LOGIN_PERSON =  ip+"/api/auth/login";
    public static final String URL_GET_COMPANIES = ip+"/api/companies";
    public static final String URL_ADD_COMPANIES = ip+"/api/addcompany";
    public static final String URL_DELETE_COMPANIES = ip+"/api/deletecompany/";
    public static final String URL_UPDATE_COMPANIES = ip+"/api/updatecompany";
    public static final String URL_ADD_PRODUCTS = ip+"/api/addproducts";
    public static final String URL_GET_PRODUCTS_BY_COMPANY = ip+"/api/products/";
    public static final String URL_DELETE_PRODUCTS = ip+"/api/deleteproducts/";
    public static final String URL_UPDATE_PRODUCTS = ip+"/api/updateproducts";
    public static final String URL_GET_SALESMAN = ip+"/api/get/salesman";
    public static final String URL_UPDATE_SALESMAN = ip+"/api/updatesalesman";
    public static final String URL_UPDATE_SALESMANPASSWORD = ip+"/api/updatesalesmanpassword";
    public static final String URL_ADD_SALESMAN = ip+"/api/salesman";



    public static final String URL_GET_COMPANIES_SALESMAN = ip+"/api/getcompanies";
    public static final String URL_GET_CLIENTS_SALESMAN = ip+"/api/getclients/";
    public static final String URL_ADD_CLIENTS_SALESMAN = ip+"/api/addclient";


    AlertDialog dialog;




    //sender_num  language
    public static String gettoken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("token", "").length() > 0) {
            return prefs.getString("token", "");
        } else
            return "";
    }

    public static String getcid(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("cid", "").length() > 0) {
            return prefs.getString("cid", "");
        } else
            return "";
    }


    public static String getroleid(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("role_id", "").length() > 0) {
            return prefs.getString("role_id", "");
        } else
            return "";
    }

    public static String getLoginState(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        if (prefs.getString("LOGIN", "").length() > 0) {
            return prefs.getString("LOGIN", "");
        } else
            return "";
    }


    public static void clearshareprefrence(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public void jobcost_confirmation(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        final AlertDialog.Builder mBuilder = new AlertDialog.Builder(context);
        View mView = inflater.inflate(R.layout.dialog_jobamount, null);
        mBuilder.setCancelable(false);


        Button btn_accept_amount = (Button) mView.findViewById(R.id.btn_accept_cost);
        Button btn_cancel_job = (Button) mView.findViewById(R.id.btn_cancel_job);
        TextView tv_jobCost = mView.findViewById(R.id.tv_jobCost);
        String cost = "60";
        tv_jobCost.setText("" + cost);
        mBuilder.setView(mView);

        dialog = mBuilder.create();
        dialog.show();
        btn_cancel_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();

            }
        });
        btn_accept_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

    }




}
