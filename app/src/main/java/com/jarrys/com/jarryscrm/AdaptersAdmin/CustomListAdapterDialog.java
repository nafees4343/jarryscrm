package com.jarrys.com.jarryscrm.AdaptersAdmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.R;

import java.util.ArrayList;

public class CustomListAdapterDialog extends BaseAdapter  {

    private ArrayList<CompanyDetails> listData;

    private ArrayList<CompanyDetails> listDataCopy;

    private LayoutInflater layoutInflater;

    public CustomListAdapterDialog(Context context, ArrayList<CompanyDetails> listData) {
        this.listData = listData;
        this.listDataCopy = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        CompanyDetails c = listData.get(position);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_row_dialog, null);

            holder = new ViewHolder();
            holder.cname = (TextView) convertView.findViewById(R.id.cname);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.cname.setText(c.getCompany_name());

        return convertView;
    }


    static class ViewHolder {
        TextView cname;
    }

}