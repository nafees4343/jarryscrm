package com.jarrys.com.jarryscrm.Admin;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.jarrys.com.jarryscrm.AdaptersAdmin.AdapterCompaniesListRv;
import com.jarrys.com.jarryscrm.AdaptersAdmin.AdapterProductsListRv;
import com.jarrys.com.jarryscrm.AdaptersAdmin.CustomListAdapterDialog;
import com.jarrys.com.jarryscrm.Drawer;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyDetails;
import com.jarrys.com.jarryscrm.ModelAdmin.CompanyProducts;
import com.jarrys.com.jarryscrm.R;
import com.jarrys.com.jarryscrm.utils.ConfigURL;
import com.jarrys.com.jarryscrm.utils.ProgressDialogClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ViewProductsFragment extends Fragment implements View.OnClickListener,Serializable {

    ArrayList<CompanyDetails> list;
    Button btn_viewproductsaddproduct;
    TextView tv_nodata;
    RecyclerView rvproductslist;
    AdapterProductsListRv adapter;


    Dialog dialog;
    CustomListAdapterDialog clad;
    ListView lvcompanies;
    ArrayList<CompanyProducts> mproductList;
    Button btn_viewcompanyselectcompany;
    String cid;
    boolean chk = true;

    public ViewProductsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_view_products, container, false);

        rvproductslist = rootView.findViewById(R.id.rvproductslist);

        chk = false;

        rvproductslist = rootView.findViewById(R.id.rvproductslist);
        btn_viewcompanyselectcompany = rootView.findViewById(R.id.btn_viewcompanyselectcompany);
        btn_viewproductsaddproduct = rootView.findViewById(R.id.btn_viewproductsaddproduct);
        tv_nodata = rootView.findViewById(R.id.tv_nodata);

        ProgressDialogClass.showRoundProgress(getActivity(),"Loading...");
        getCompanyDetails();

        btn_viewproductsaddproduct.setOnClickListener(this);
        btn_viewcompanyselectcompany.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        if(chk) {
            Log.d("VVISBILE","agae "+chk);
            list.clear();
            if (ConfigURL.getcid(getActivity()).length() > 0) {
                Log.d("VVISasdadasdBILE","agae ");
                getProductsByCompany(ConfigURL.getcid(getActivity()));
            }

        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_viewproductsaddproduct:
                Intent i = new Intent(getActivity(),AddProductActivity.class);
                i.putExtra("companylist",list);
                startActivity(i);
                chk = true;
                break;
            case R.id.btn_viewcompanyselectcompany:
                showDialogCompanies();
                lvcompanies.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                        Log.d("listOfCompanies","Selected: "+clad.getItem(i));
                        CompanyDetails c = list.get(i);
                        btn_viewcompanyselectcompany.setText(""+c.getCompany_name());
                        cid = c.getcid();
                        getProductsByCompany(cid);
                        dialog.cancel();
                    }
                });
                break;
        }
    }

    public void getProductsByCompany(String cid) {
        ProgressDialogClass.showRoundProgress(getActivity(),"Loading...");
        Log.d("headerheader","header: "+ConfigURL.gettoken(getActivity()));
        AndroidNetworking.get(ConfigURL.URL_GET_PRODUCTS_BY_COMPANY+cid)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        getCompanyDetails();
                        Log.d("VPresponse","res:"+response);
                        boolean status = false;

                        try {
                            JSONArray jsonArray = response.getJSONArray("products");
                            Log.d("asddaagfhfd","asd:"+jsonArray.length());
                            mproductList = new ArrayList<>();
                            if(jsonArray.length()>0) {
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject data = jsonArray.getJSONObject(i);
                                    String id = String.valueOf(data.get("id"));
                                    String name = String.valueOf(data.get("name"));
                                    String picture = ConfigURL.ip + (String) data.get("picture");
                                    String unit_price = (String) data.get("unit_price");
                                    String quantity = (String) data.get("quantity");
                                    Integer company_id = data.getInt("company_id");
                                    Integer user_id = data.getInt("user_id");

                                    SharedPreferences preferences = getActivity().getSharedPreferences("PREFRENCE", Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = preferences.edit();
                                    editor.putString("cid", ( String.valueOf(company_id)));
                                    editor.commit();

                                    Log.d("asdaasdasdasdaas", "id: " + id + "\nname: " + name + "\nImage: " + picture + "\nunit_price: " + unit_price + "\nquantity: " + quantity + "\ncompany_id: " + company_id + "\nUserId: " + user_id);

                                    CompanyProducts companyProducts = new CompanyProducts(id, name, picture, unit_price, quantity, company_id);
                                    mproductList.add(companyProducts);

                                    adapter = new AdapterProductsListRv(getActivity(), mproductList, new AdapterProductsListRv.MyAdapterListener() {
                                        @Override
                                        public void iconTextViewOnClick(View v, int position) {
                                            Log.d("ProductDelete", "iconTextViewOnClick at position " + position);
                                            chk = true;
                                        }
                                    });

                                    rvproductslist.setAdapter(adapter);
                                    rvproductslist.setHasFixedSize(true);
                                    rvproductslist.setItemViewCacheSize(20);
                                    rvproductslist.setDrawingCacheEnabled(true);
                                    rvproductslist.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                    LinearLayoutManager llm = new LinearLayoutManager(getActivity());
                                    rvproductslist.setLayoutManager(llm);
                                }
                            }
                            else {
                                Log.d("anannaa","aya:"+jsonArray.length());
                                mproductList.clear();
                                adapter = new AdapterProductsListRv(getActivity(), mproductList, new AdapterProductsListRv.MyAdapterListener() {
                                    @Override
                                    public void iconTextViewOnClick(View v, int position) {
                                        Log.d("ProductDelete", "iconTextViewOnClick at position " + position);
                                        chk = true;
                                    }
                                });
                                rvproductslist.setAdapter(adapter);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(ViewActivity.this, "code:" , Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        ProgressDialogClass.dismissRoundProgress();
                        Toast.makeText(getActivity(), "Some thing went wrong" , Toast.LENGTH_LONG).show();

                    }
                });
    }

    private void showDialogCompanies(){

        dialog = new Dialog(getActivity());

        View view = getLayoutInflater().inflate(R.layout.dialog_main, null);

        lvcompanies = (ListView) view.findViewById(R.id.custom_listofcompanies);

        if(list!=null) {
            list.removeAll(Arrays.asList(null, ""));
        }

        Log.d("sizeee","size: "+list.size());

        // Change MyActivity.this and myListOfItems to your own values
        clad = new CustomListAdapterDialog(getActivity(),  list);

        lvcompanies.setAdapter(clad);

        dialog.setCancelable(true);

        dialog.setContentView(view);

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();

    }

    public void getCompanyDetails() {
        Log.d("headerheader","header: "+ConfigURL.gettoken(getActivity()));
        AndroidNetworking.get(ConfigURL.URL_GET_COMPANIES)
                .addHeaders("Authorization","bearer "+ConfigURL.gettoken(getActivity()))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        ProgressDialogClass.dismissRoundProgress();
                        Log.d("VPCresponse","res:"+response);
                        boolean status = false;

                        //String img_tag = "http://goodwisesearch.com/uploads/";

                        try {
                            JSONArray jsonArray = response.getJSONArray("companies");
                            Log.d("asddaagfhfd",""+jsonArray.length());
                            list = new ArrayList<>();
                            for(int i=0; i<jsonArray.length(); i++) {
                                JSONObject data = jsonArray.getJSONObject(i);
                                String id = String.valueOf(data.getInt("id"));
                                String company_name = (String) data.get("company_name");
                                String company_description = (String) data.get("company_description");
                                String company_image = ConfigURL.ip+(String) data.get("company_image");
                                String company_phone = (String) data.get("company_phone");
                                String company_address = (String) data.get("company_address");
                                String company_email = (String) data.get("company_email");
                                String user_id = String.valueOf(data.get("user_id"));;

                                Log.d("asdadas","Name: "+company_name+"\nDescription: "+company_description+"\nImage: "+company_image+"\nPhone: "+company_phone+"\nAddress: "+company_address+"\ncemail: "+company_email+"\nUserId: "+user_id);

                                CompanyDetails companyDetails = new CompanyDetails(id,company_name,company_description,company_image,company_phone,company_address,company_email,user_id);
                                list.add(companyDetails);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            //Toast.makeText(ViewActivity.this, "code:" , Toast.LENGTH_LONG).show();

                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getActivity(), "Some thing went wrong" , Toast.LENGTH_LONG).show();

                    }
                });
    }
}
